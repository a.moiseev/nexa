Release Notes for Nexa 1.1.0
======================================================

Nexa version 1.1.0 is now available from:

  <https://gitlab.com/nexa/nexa/-/releases>

Please report bugs using the issue tracker at github:

  <https://gitlab.com/nexa/nexa/-/issues>

This is minor release of Nexa, for more information about Nexa see:

- https://nexa.org
- https://spec.nexa.org

Upgrading
---------

Main changes in 1.1.0
-----------------------

This is list of the main changes that have been merged in this release:

- Greatly improve locking for a lot of RPC calls
- Drop OpenSSL as source of randomness
- Improve handling of priority network messages
- Fix a rare bug that won't let you broadcast wallet transactions
- Increase efficiency on node shutdown
- Update nexad docker files
- Fix retrieval of transaction by txidem
- Code clean ups and improvements all over the codebase
- Rostrum 8.0


Commit details
--------------

- `e2f15c2de` Bump nexa version to 1.1.0 (Andrea Suisani)
- `4ff1e6f72` Fix for wallet bug: txns sometimes not getting relayed (Peter Tschipper)
- `954b48fef` Reduce the scope of the tx admission pause when we process a block (Peter Tschipper)
- `c4e309a06` Protect inbound fClients from getting disconnected (Peter Tschipper)
- `d6e2caed3` Improve shutdown performance when many peers connected (Peter Tschipper)
- `5198478f4` Take out NetMsgType::BLOCK from IsPriority() (ptschip)
- `485c6f6b0` Remove cs_main lock in getblockstats (Peter Tschipper)
- `aec1b8891` Remove the copy of vNodes in CleanupDisonnctedNodes() (ptschip)
- `394193f5c` Remove cs_main lock for getblockchaininfo (Peter Tschipper)
- `61a0fc701` Add new testblocks to test_nexa.cpp and re-enable some bloom tests. (Peter Tschipper)
- `93b815540` Use a dedicated lock for nLastBlockSize and nLastBlockTx (ptschip)
- `70f65d1c7` add libgmp to the dependencies document (Andrew Stone)
- `642e4a8eb` move connection related expedited stuff into the connection manager (where other expedited connection stuff already exists) (Andrew Stone)
- `862db6564` test: Fetch & run electrum tests from rostrum repo (Dagur Valberg Johannsson)
- `0142af380` Update docker files to work with nexa (Andrea Suisani)
- `17e04802c` fix 2 rare core dumps; first an unlikely race condition in chainActive access, second an elusive crash when repeatedly accessing the args dictionary (Andrew Stone)
- `51561851b` Fix issue with signing and verifying messages, with templated addresses, through the QT interface. (Peter Tschipper)
- `8bb64e4bc` show status and whether the header/block is on the main chain.  fix small issue in the LINKED transition from no tx to an actual flag (Andrew Stone)
- `694aab165` Store transactions as CDataStream objects in mapRelay (Peter Tschipper)
- `dd90e6127` Comment out a noisy debug message in the QT debug ban list (ptschip)
- `54edbb3a7` fix the loading of the blockindex (Peter Tschipper)
- `d3667e5e7` Remove cs_main lock from the peertablemodel when getting node stats (Peter Tschipper)
- `a2d48fb71` Fix txindex when doing lookups by idem (Peter Tschipper)
- `0a494f94e` Don't add `fee` filed to transaction json in case of coinbase transaction (Andrea Suisani)
- `6e002ad23` Change the default blockfile size from 2GB to 128MB (Peter Tschipper)
- `d64df3d37` Reduce the default max outbound connections from 16 to 8 (Peter Tschipper)
- `b0c1ef4b6` Remove old unused code for graphene and thinblocks (ptschip)
- `955a20133` Make further attempts at removing noderefs from the priority queues (ptschip)
- `e06d54c8b` Port Bitcoin Core random number seeding to Nexa (partial port of Core PR #17265 and following changes).
- `56ec5ef12` Make rpc-tests properly execute electrum tests when explicitly list at command line (Andrea Suisani)
- `690b9145e` Properly compute coinbase reward after each halving (Andrea Suisani)
- `e7977d474` Fix reporting for number of transactions in the wallet (Peter Tschipper)
- `cfc0a5ff6` Only check if data is sorted in debug mode (ptschip)
- `b218a138d` Turn block file size into a chainparam (Peter Tschipper)
- `7e3ea4376` Modify blockchain.py qa test to take into account the new coinsupply field (Andrea Suisani)
- `b276a8454` Replace cs_main lock with cs_chainLock when processing GETHEADERS p2p request (Peter Tschipper)
- `dae6bac2d` Use GetCoinsMinted() to provide value for coinsupply field (Andrea Suisani)
- `6c41c1a05` Add GetCoinsMinted() (Andrea Suisani)
- `dc277b2b3` add total coinsupply to getblockchaininfo (rpc) (Proteus)
- `7fecbb768` [build] Fix libboost and libevent detection / CPPFLAGS on native macos (Andrea Suisani)
- `a0d5d3d58` getnetworkhashps should estimate network hashrate on a 20 hrs interval (Andrea Suisani)
- `d551c4fed` fix small issue in cashlib group encoding (Andrew Stone)
- `debaec360` Allow fetching blocks outside active chain via RPC (Dagur Valberg Johannsson)
- `c991b8d42` fix ProcessOrphans core dump (ptschip)
- `129c91c88` remove extern for non-existent tweak (ptschip)
- `eaa3e0801` Add a python tests for long chains and long chains of orphans (Peter Tschipper)
- `992750e0f` qa: Add first set of electrum token tests (Dagur Valberg Johannsson)
- `1f5567b1a` Simplify GuessVerificationProgress() so that does not relay on manually updating checkpoint data (Peter Tschipper)
- `7334322ea` Fix block sync when we launch against an older block store (Peter Tschipper)
- `1b66d4b0a` Fix an occasional core dump on shutdown (ptschip)
- `a7b65b3b0` Prevent nullptr dereference in various rpc's (Peter Tschipper)
- `6a35020e9` Remove nChainTx as a flag for whether the block is linked to the chain.  Use IsLinked() instead (Peter Tschipper)
- `b7d597127` Update help text for set() tweaks (ptschip)
- `ba2cd9aab` Reapply !117  (electrum: Check if token outputs are in history) (Andrea Suisani)
- `0aca07b2a` If the txn has exceeded the limits for inputs/ouputs then give error (Peter Tschipper)

Credits
=======

Thanks to everyone who directly contributed to this release:

- Andrea Suisani
- Andrew Stone
- Dagur Valberg Johannsson
- Peter Tschipper
- Proteus

We have backported a set changes from Bitcoin Core.

Following all the indirect contributors whose work has been imported via the above backports:

- fanquake
- Pieter Wuille
- MacroFake
- pasta
- Wladimir J. van der Laan

